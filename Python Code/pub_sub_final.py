import time
from umqtt.simple import MQTTClient
import ubinascii
import machine
import micropython
import network
import esp
from time import sleep
from umqtt.simple import MQTTClient
from dht import DHT11
from machine import TouchPad, Pin
#esp.osdebug(none)
import gc
gc.collect
pin = (Pin(33, Pin.OUT, value=0))


server = '192.168.43.202' #MQTT Server Adress
client_id = 'ESP32'
sub_test = 'irrigation'

server = '192.168.43.202' 
TOPIC_t = b'temp'
TOPIC_h = b'hum'
TOPIC_cap1 = b'cap1'
TOPIC_cap2 = b'cap2'
TOPIC_rain = b'rain'
sub_test = 'irrigation'

client = MQTTClient(client_id, server, user='username', password='password')
client.connect()

sensor = DHT11(Pin(15, Pin.IN, Pin.PULL_UP))

sensor_touch1 = TouchPad(Pin(4))
sensor_touch1.config(450)
#sensor_touch2 = TouchPad(Pin(2))    for second measuring point
#sensor_touch2.config(200)
rainpin = Pin(25, Pin.IN)



def sub_cd(topic, msg):
    print((topic, msg))
    pin.value(0)
    time.sleep(10)
    pin.value(1)
    
def connect_and_subscribe():
    global client_id, server, sub_test
    client = MQTTClient(client_id, server, user='username', password='password')
    client.set_callback(sub_cd)
    client.connect()
    client.subscribe(sub_test)
    
    
    print('connect to %s Mqtt broker, subscribed to %s topic' % (server, sub_test))
    return client

def restart_and_reconnect():
    print ('Failed to connect to MQTT Broker.Reconnecting...')
    time.sleep(10)
    machine.reset()
    
try:
    client = connect_and_subscribe()
except OSError as e:
    restart_and_reconnect()
    
while True:
    try:
        sensor.measure()
        t = sensor.temperature()
        h = sensor.humidity()
        sensor_touch1.read()
        #sensor_touch2.read()
        cap1 = sensor_touch1.read()
        #cap2 = sensor_touch2.read()
        rain = rainpin.value()
        
        if isinstance(t, int) and isinstance(h, int) and isinstance(cap1, int) and isinstance(rain, int):
            msg_t = ('{0:3.0f}'.format(t))
            msg_h = ('{0:3.0f}'.format(h))
            client.publish(TOPIC_t, msg_t)
            client.publish(TOPIC_h, msg_h)
            print(msg_t)
            print(msg_h)
            msg_cap1 = ('{0:3.0f}'.format(cap1))
            client.publish(TOPIC_cap1, msg_cap1)
            print(msg_cap1)
            #msg_cap2 = ('{0:3.0f}'.format(cap2))
            #client.publish(TOPIC_cap2, msg_cap2)
            #print(msg_cap2)
            msg_rain = ('{0:3.0f}'.format(rain))
            client.publish(TOPIC_rain, msg_rain)
            print(msg_rain)
            
            try:
                new_message = client.check_msg()
                client.check_msg()
                if new_message != None:
                    time.sleep(1)
            
            except OSError as e:
                restart_and_reconnect()
            
        else:
          print('Invalid sensor readings.')
        
    
        
    except OSError:
        print('Failed to read sensor.')
        
    sleep(4)
        
