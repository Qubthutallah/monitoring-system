# Monitoring System

A control system for a PV-powered stand alone irrigation system. Further it monitors the system and sends an alert message via e-mail in case of failures. System consists of a RaspberryPi (mqtt-Broker) and a ESP32 (client) connected within same WiFi.