import machine
import esp32
import umqtt.simple
import network

WiFi_SSID = "esp32_test"
WiFi_PASS = "elo98765"

def do_connect():
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    if not wlan.isconnected():
        print('connecting to network...')
        wlan.connect(WiFi_SSID, WiFi_PASS)
        while not wlan.isconnected():
            pass
    print('network config:', wlan.ifconfig())
    
do_connect()

import pub_sub_final.py

